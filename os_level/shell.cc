#include "shell.hh"

int main()
{ std::string input;

  std::string prompt = "";

  int promptfile = syscall(SYS_open, "configuratie.txt", O_RDONLY, 0755);
  char byte[1];                                         
  while(syscall(SYS_read, promptfile, byte, 1))
    prompt += byte;

  syscall(SYS_close, promptfile);

  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() {
  std::string filenaam;
  std::string fileline;
  std::string filetotaal;

  std::cout << "Voer filenaam in: ";
  std::cin >> filenaam;

  std::cout << "Voer tekst in: ";
  while(std::getline(std::cin, fileline)){
    if(fileline == "<EOF>"){
      break;
    }
    filetotaal += fileline;
  }

  const char* filenaam_cstring[] = {filenaam.c_str()};
  const char* filetekst_cstring[] = {filetotaal.c_str()};

  int filedescriptor = syscall(SYS_creat, filenaam_cstring[0], 0755);

  syscall(SYS_write, filedescriptor, filetekst_cstring[0], filetotaal.size());
  syscall(SYS_close, filedescriptor);
}

void list() { 
  const char* args[] = {"/bin/ls", "-l", "-a", NULL};
  int procesid = syscall(SYS_fork);
  if(procesid == 0){
    syscall(SYS_execve, args[0], args, NULL);
  }else{
    syscall(SYS_wait4, 0, NULL, NULL, NULL);
  }

}

void find() { 
  std::string invoer;

  std::cout << "Voer string in om op te zoeken: ";
  std::cin >> invoer;

  const char* argsFind[] = {"/usr/bin/find", ".", NULL};
  const char* argsGrep[] = {"/bin/grep", invoer.c_str(), NULL};

  int fd[2];
  syscall(SYS_pipe, &fd);

  int procesidFind = syscall(SYS_fork);
  if (procesidFind == 0){
    syscall(SYS_close, fd[0]);
    syscall(SYS_dup2, fd[1], 1);

    syscall(SYS_execve, argsFind[0], argsFind, NULL);
  }
  else{
    int procesidGrep = syscall(SYS_fork);
    if (procesidGrep == 0){
      syscall(SYS_close, fd[1]);
      syscall(SYS_dup2, fd[0], 0);

      syscall(SYS_execve, argsGrep[0], argsGrep, NULL);
    }
    else{
        syscall(SYS_close, fd[0]);
        syscall(SYS_close, fd[1]);

        syscall(SYS_wait4, procesidGrep, NULL, NULL, NULL);
        syscall(SYS_wait4, procesidFind, NULL, NULL, NULL);
    }
  }
}

void seek() {  
  int fd_seek = syscall(SYS_creat, "seek", 0755);

  syscall(SYS_write, fd_seek, "x", 1);

  int seekEind = syscall(SYS_lseek, fd_seek, 5000001, SEEK_SET);
  
  syscall(SYS_write, fd_seek, "x", 1);
  syscall(SYS_close, fd_seek);


  int fd_loop = syscall(SYS_creat, "loop", 0755);

  std::string loopInhoud = "x";
  for(unsigned int i = 0; i < 5000000; i++){
    loopInhoud += '\0';
  }
  loopInhoud += 'x';

  const char* loop_cstring[] = {loopInhoud.c_str()};
  syscall(SYS_write, fd_loop, loop_cstring[0], loopInhoud.size());
  syscall(SYS_close, fd_loop);
  
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
