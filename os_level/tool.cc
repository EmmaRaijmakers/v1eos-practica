#include <iostream>
#include <string>

std::string translate(std::string line, std::string argument){ 
  std::string result = ""; 
  int movePlaces = std::stoi(argument);
  for(unsigned int i = 0; i < line.size(); i++){ 
    char newLetter = line[i] + movePlaces;
    if(line[i] >= 'a' && line[i] <= 'z'){
      if(newLetter > 'z'){ 
        newLetter = newLetter - ('z' - 'a') - movePlaces + 1;
      }
      else if(newLetter < 'a'){
        newLetter = newLetter + ('z' - 'a') - movePlaces - 1; 
      }
      result += newLetter;
    }
    else if(line[i] >= 'A' && line[i] <= 'Z'){
      if(newLetter > 'Z'){
        newLetter = newLetter - ('Z' - 'A') - movePlaces + 1;
      }
      else if(newLetter < 'A'){
        newLetter = newLetter + ('Z' - 'A') - movePlaces - 1;
      }
      result += newLetter;
    }
  }
  return result; 
}

int main(int argc, char *argv[])
{ std::string line;

  if(argc != 2)
  { std::cerr << "Deze functie heeft exact 1 argument nodig" << std::endl;
    return -1; }

  while(std::getline(std::cin, line))
  { std::cout << translate(line, argv[1]) << std::endl; } 

  return 0; }
